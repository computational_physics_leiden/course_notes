### Project 1 - Molecular dynamics

Classes:
* Feb 8 - intro: interactions, PBC, integrating equations of motion
* Feb 15 - dimensionless units, tricks for minimum image convention
* Feb 22 - Velocity-Verlet
* Mar 1 - Initial conditions (positions/velocities/temperature), observables: pair correlations, pressure, specific heat)
* Mar 8 - *no course on this day!*
* Mar 15 - estimating errors
* Mar 22 - individual consultation

**Deadline for project: Mar 29**

*End of the 3ECTS course*

### Project 2: Monte Carlo

Classes:
* Mar 29 - intro to Monte Carlo, intro to individual projects
* April 5 - individual consultation
* April 12 - individual consultation

**Deadline for project: April 26**

### Project 3: Individual projects

Classes:
* April 26 - choice of individual project, consultations
* May 3  - individual consultations
* May 10 - individual consultations
* **TENTATIVE: May 24 - presentations**

*End of the 6ECTS course*