# Project 2 - Monte Carlo simulations

## Introduction

Many problems in statistical mechanics involve a very large phase space, and
computing an observable usually implies integrating over this large phase space -
which typically becomes impossible as the problem size grows.

Monte Carlo methods are widely used computational methods that evaluate
a multi-dimensional integral stochastically, instead of attempting
an exact solution. In this project you will explore one example
of these methods in detail.

## Instructions

Choose one out of three different projects that involve a Monte Carlo simulation:

### Project 2A - Monte Carlo simulations for the Ising model

The Ising model is a simple model for (anti)-ferromagnetism. In 2D, it consists of spins sitting on a
square lattice, where the spins $`s_i`$ can take values $`+1`$ or $`-1`$. The Hamiltonian is given as

```math
H = - J \sum_{\langle ij\rangle} s_i s_j - H \sum_i s_i
```

where $`\langle i j \rangle`$ denotes a sum over nearest neighbors (horizontal and vertical) on the lattice. $`J>0`$
favors ferromagnetism, $`J<0`$ antiferromagnetism, and $`H`$ represents an external magnetic field.

The 2D Ising model can actually [be solved analytically](https://en.wikipedia.org/wiki/Square-lattice_Ising_model), and shows a phase transition from non-magnetic to ferromagnetic (if $`J>0`$) as a function of temperature.

Write a Monte Carlo simulation to study the thermodynamic properties of a $`n\times n`$ lattice. This can be done
using the Metropolis Monte Carlo algorithm introduced in the lecture. A Monte Carlo move in the simplest case would consist of picking a spin and flipping its values. (This is good as a starting point, but more advanced algorithms are available, see below). 

Of particular interest is to compute the average magnetization $`\langle m\rangle = \frac{1}{N} \langle \sum_i s_i\rangle`$, the specific heat per spin $`c = \frac{k_B \beta^2}{N} \left( \langle E^2 \rangle - \langle E \rangle^2 \right)`$,
and the susceptibility per spin $`\chi = \beta N \left( \langle m^2 \rangle - \langle m \rangle^2 \right)`$
(use your thermodynamics knowledge to convince you that those formulas are correct). 

As a first step, write a Monte Carlo program computing the average magnetization and $`\chi`$ or $`c`$ as a function of temperature to find the phase transition. Possible further steps are (sorted in arbitrary order, not necessarily importance):

- implement the heat bath algorithm. This also satisfies the detailed balance criterion, but is faster (has a shorter correlation time)
- implement a parallelized/numpified version of the Metropolis algorithm (google for "checkerboard algorithm").
- implement the Swendsen-Wang or Wolff algorithm. The conventional Metropolis algorithm suffers from critical
slow-down near the phase transition, these more advanced algorithms overcome this limitation. 
- include the effect of an external magnetic field

Literature: The basics of the Ising model are explained in [Chapter 7 of Jos' book](../project 1/background_reading/Review statistical mechanics.pdf), the Monte Carlo simulation
of the Ising model in [Chapter 8](background_reading/TheMonteCarloMethod.pdf), and the advanced algorithms in [Chapter 15.5](background_reading/ComputationalMethodsLatticeFieldTheory.pdf).

### Project 2B - Variational Quantum Monte Carlo

The stationary Schrödinger equation can only be solved exactly for some specific examples. In general, and certaily
for interacting systems, an exact solution is impossible. Still, we can try to find the approximate ground state energy using a variational calculation. 

To this end, we consider a *trial wave function* $`\psi_T(\mathbf{r}, \alpha)`$, where $`\alpha`$ is one or more
variational parameters. We can compute the expectation value of the energy as 

```math
E(\alpha) = \frac{\int d\mathbf{r}\, \psi_T^*(\mathbf{r},\alpha)\, H\, \psi_T(\mathbf{r}, \alpha)}{\int d\mathbf{r}\, \psi_T^*(\mathbf{r},\alpha) \psi_T(\mathbf{r}, \alpha)}
```

The variational principle tells that $`E(\alpha) \geq`$ the real ground state energy. The best approximation to the
ground state energy is thus obtained by minimizing $`E(\alpha)`$ with respect to $`\alpha`$. Obviously, the quality
of the approximation depends on how good the trial wave function is.

The Monte Carlo method is in this case used to compute the integral over the degrees of freedom of the wave function
$`\mathbf{r}`$. To get a proper probability, we define the local energy as 

```math
E_\mathrm{loc}(\mathbf{r}, \alpha) = \frac{H \psi_T(\mathbf{r}, \alpha)}{\psi_T(\mathbf{r}, \alpha)}
```

Then the energy can be computed as 

```math
E(\alpha) = \frac{\int d\mathbf{r}\, \psi_T^*(\mathbf{r},\alpha)\psi_T(\mathbf{r}, \alpha)\, E_\mathrm{loc}(\mathbf{r}, \alpha)}{\int d\mathbf{r}\, \psi_T^*(\mathbf{r},\alpha) \psi_T(\mathbf{r}, \alpha)}
```

and we can solve this integral using Monte Carlo integration with importance sampling according to the probability density $`\psi_T^* \psi_T/\int d\mathbf{r} \psi_T^* \psi_T`$. 

As a start, apply the variational Monte Carlo method on a solvable model, such as a harmonic oscillator. You will
need to

- derive the expression for the local energy for a given trial wave function
- write a code that can implements the Monte carlo integration
- minimize the energy with respect to $`\alpha`$. This is a bit more subtle as it seems, as the Monte Carlo result for $`E(\alpha)`$ has an error associated with it, and the usual minimization algorithms may be thrown off track. Care must be thus taken to compute the gradient.

After that, solve the problem for the helium atom for which no exact analytical solution is given. 

Literature: Details on the variational method can be found in [Chapter 4 of the lecture notes on advanced quantum mechanics](background_reading/aqm.pdf) and implementation details of the Monte Carlo method in [Chapter 12 of Jos' book](background_reading/QuantumMonteCarlo.pdf).

### Project 2C - Monte Carlo simulations of polymers

The third option is a bit different from the other two on the technical side:
instead of using the Metropolis algorithm (i.e. generating a Markov chain)
to sample the Boltzmann distribution, we use a nearby distribution for the
importance sampling.

The goal is to simulate the properties of long polymers in a good
solvent (the latter just means that the polymers do not want to stick
to other polymers in the solution, and thus we can simulate individual
polymers). We use a simple model for the polymer in 2D

- it consists of a sequence of $`N`$ consecutive "beads"
- the distance between beads is fixed (1 in dimensionless units)
- the angle $`\theta`$ between the links of three neighboring beads can take
  any value
- the interaction between the beads/atoms is modelled with a Lennard-Jones
  potential

If there was no interaction between atoms, the polymer would correspond to
a random walk. The repulsive interaction of the Lennard-Jones potential
prevents atoms to come too close and turns the polymer into a *self-avoiding
random walk*.

![](build_polymer.png)

We could sample the different polymers by choosing all angles
completely randomly. The problem with this approach is that most
polymers would energetically be very unfavorable. The Rosenbluth
algorithm improves on this by building up a polymer step by step: a
new bead with angle $`\theta`$ is added to an existing polymer with
probability $`\sim \exp(-E(\theta)/k_b T)`$, where $`E(\theta)`$ is
the additional energy due to the extra bead. Note that while this looks like
the Boltzmann distribution, the full polymer does not go according to the
Boltzmann distribution! The Monte Carlo sampling
then consists of growing an ensemble of polymers (which are completely
independent, unlike the Markov chain).

To account for the deviation from the Boltzmann sampling, we
have to associate a weight factor $`W_i`$ with the $`i`$-th sample.
The expectation value of a quantity $`A`$ is then given by

```math
\langle A\rangle = \frac{\sum_i W_i A_i}{\sum_i W_i}
```

Work out the weight factors by consulting [Chapter 10.6 from Jos' book!](background_reading/TheMonteCarloMethod.pdf)

An interesting quantity to study is the end-to-end distance of the
polymer. Compare the case with interactions to the case without
interactions between the atoms.

You will find that for long polymers very few samples have a large weight and
dominate, giving a big error on the result. You can fix this problem by
using a more advanced sampling method, called PERM.

Literature: [Chapter 10.6 from Jos' book](background_reading/TheMonteCarloMethod.pdf)

## Resources

- Materials covered in the lectures. These are also summarized in the
  lecture notes.
- [Chapters 10](background_reading/TheMonteCarloMethod.pdf)
  [12](background_reading/QuantumMonteCarlo.pdf), and [15](background_reading/TheMonteCarloMethod.pdf) from the book
  "Computational Physics" by Jos Thijssen
- [Chapter 4 of the Advanced Quantum Mechanics lecture notes](background_reading/aqm.pdf)
- Feel free to search for any help/code snippets/ideas you can find on
  the internet. We definitely encourage you to use Numpy/Scipy/...!

## Products

* A report containing:
  - a summary of the theoretical background of the simulations,
  - relevant information about your implementation (including validation),
  - a systematic and structured overview of your simulations, and
    a critical discussion of the physics you find.
* The final version of your code.

## Assessment criteria

** To be added soon! **

## Supervision and help

* The lecturers and the TAs are present during the lectures and are
  willing to help with all your problems
* Out of class you are encouraged to ask any question on the course
  chat (preferred), write an email to the lecturers/TAs, or drop by the office.

## Submission and feedback

**Submission date for report and code: April 26**

Please submit your report and the code to the teachers and the teachng assistant via email.
Submit
* the report as a PDF file
* We expect the code to be handed in as both a set of Python files and a PDF file ready to print. 
  The Python code will be used to see whether your code runs and gives correct results. The PDF file
  will be used for grading, and should be typeset using LaTeX and the listings or minted packages,
  and must include line numbers. Keywords should be somehow highlighted (the default options are fine),
  but don't go overboard with the colours. (You can find an example [here](howtos/typesetting.md))

If you submit a draft version of your report to the lecturers before April 23,
we will give you a short feedback containing the most important aspect to
improve in your report before April 24.