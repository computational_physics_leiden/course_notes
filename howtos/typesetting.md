To typeset your code, you can use the aptly named file [typeset_code.tex](typeset_code.tex). 

Copy-paste the correct filenames (with the correct paths) into the lstinputlisting command
(one for each file, add more if you need to), and compile using the command 'pdflatex typeset_code.tex', 
which outputs a file called 'typeset_code.pdf'. Alternatively, you can use you favourite LaTeX IDE. 

Then change the filename to something more descriptive, e.g. the project name and
include your names and/or student numbers. 

Instructions on typesetting using the Listings package can be found here: https://en.wikibooks.org/wiki/LaTeX/Source_Code_Listings. 
If you want to use a different package, there are instructions for that too. Just make sure that every line has a line number.