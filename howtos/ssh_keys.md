## Add a ssh-key to your account

Gitlab allows to clone repositores in HTTPS or SSH mode. The latter has the advantage that you don't always have to enter your password when you do changes, but requires you to set up your ssh keys:

1. Open a terminal on the computer you want to use git. If you are using the compute server,
   click on "New" -> "Terminal"
2. If you don't have a ssh key yet, type in `ssh-keygen` and hit ENTER. First you will be asked
   to enter the file, simply hit ENTER to use the standard location. You will be further asked to
   enter a password (If you use the key only for the course and our gitliab, it is safe enough
   to have an empty password)
   ![](images/ssh-keygen.png)
3. Execute `cat .ssh/id_rsa.pub` and copy the complete text
   ![](images/copy-key.png)
   Note that on the compute server you need to use the copy function of the browser (CTRL-C 
   won't work).
4. Go to https://gitlab.kwant-project.org/profile/keys (sign in if you aren't yet). Paste the
   copied key into the box for the key. Add some title (not important what), and click on
   "Add key".
   IMPORTANT NOTE - if you did the copy in the browser window for the compute server, there is
   a little inconvenience (this does *not* apply if you copied in a terminal on your computer):
   The copy also has copied all the line breaks in the terminal window, although it shouldn't have.
   Compare the output in the terminal window and the pasted key, identify the corresponding line
   breaks and remove them. (There might be other line breaks at whitespace, do not remove those!)
   ![](images/remove_linebreaks.png)

If you are using your own computer, and cannot get ssh keys to work, please revert to using gitlab cloning with HTTPS.