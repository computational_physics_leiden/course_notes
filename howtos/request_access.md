### Request access to computational group

1. Go to https://gitlab.kwant-project.org/computational_physics_18_leiden

2. Click on "Request Access"
![](access_to_group.png)

3. A course instructor needs to approve your access request, which typically happens fast (if not, you know who to bug). An email will confirm your access to the group.