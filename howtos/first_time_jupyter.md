For the course we offer a hosted python environment with all the necessary packages installed (of course you can also use your own local python installation!). Here we show the first time set up of this service.

**IMPORTANT** These steps will only work *after* you have been granted access to the `computational_physics_18` group!

1. Go to https://compphys.quantumtinkerer.tudelft.nl

2. Click on "Sign in with Gitlab"

3. If you were not logged in in gitlab yet, you will be redirected to
the gitlab log in page. Log in with your account.

4. When you do this for the first time, you will be asked to allow "TU Delft Computational Physics" use your gitlab account. Click on "Authorize"
![](allow_to_use_account.png)

5. After that you will be redirected to the jupyterhub server website. Click on "Start My Server" to start your own instance.
![](start_my_server.png)

6. Finally, you will come to your own juptyerhub home screen
![](blank_jupyterhub.png)