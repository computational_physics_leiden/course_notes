# Assessment criteria

## Code (30%)

*  **Code fulfils specification (15%)**

   The code is a working molecular dynamics simulation (yes/no)

   We assess if the code is working by inspection and checking if you computed at least one
   meaningful physical obervable and presented it in your report. If the code is not working
   we will not grade your project.
  
*  **Documentation (5%)**

   Functions and modules are documented adequately
   (i.e. includes all necessary details, but is not excessively long)
  
*  **Structure (5%)**

   -   the code is separated into functions (or classes) according to
       the simulation logic.
   -   the generic core of the simulation is separated from the code running
       the simulations for specific parameters
   -   the only global variables are physical or simulation constants
 
*  **Readability (5%)**

   -   variable and function names are understandable and follow a logical scheme
   -   a consistent formatting and nameing scheme is used
   -   nontrivial (and only nontrivial) parts of the code are commented


## Report (70% + 10% bonus)

*  **Theoretical background (10%)**

   The theoretical background of the simulation is adequately explained

*  **Report structure (10%)**

   The report has a structure fulfilling scientific standards: abstract,
   introduction/background, methods, results, summary, and key references.

*  **Presentation of the simulation results (10%)**

   Results of the simulations are properly presented and the physical meaning
   correctly discussed. Figures are complete with proper axis labeling and captions.

*  **Validation of the simulation (10%)**

   The correctness of the simulation is tested properly, e.g. by comparing to
   known literature results.

*  **Calculated quantities (30% + max. 10% bonus)**

   -  *Pair correlation function (15%)*

      Pair correlation function in the three states of matter.

   -  *One additional observable (15%)*

      Calculate at least one of pressure, specific heat, diffusion properties
      or another interesting observable. Investigate the parameter dependence
      and give error bars.

   -  *Bonus (max 10%)*

      Calculate additional meaningful quantities and analyse them properly
      (e.g. another observable mentioned above, or something you come up with
      yourself)