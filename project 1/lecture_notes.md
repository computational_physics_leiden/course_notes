# Project 1: Molecular dynamics

## List of problems and questions you will encounter in the project

* How to keep your code readable?
* How to avoid (or minimize the usage) of Python loops and implementing the algorithm in numpy?
* How to efficiently compute the minimum-image convention?
* How to ensure conservation of energy?
* How to structure your code (it should be easy to change the parameters/loop over them/compute different observables)?
* How to set the temperature?
* How to compute the error in a quantity such as the kinetic energy so that this does not depend on the time-step?
* How to compute physical quantities:
  - Correlation function
  - Specific heat
  - Pressure
  
  
### Project goals

In this project we want to simulate phase transitions in materials: under which circumstances is the material solid, liquid or a gas?

In order to do this, we will simulate the classical motion of particles (for simplicity) in the material. One of the first choices is which material: water for example is very complicated, as it forms hydrogen bonds, the molecule is very asymmetric, etc. We want something simpler, and a noble gas is such a reasonable choice (we don't have to worry about formation of molecules, for example). Historically, Argon (Ar) is the best studied and this is what we will, too.

In a simulation, we will have to consider different aspects:

### How do Argon atoms interact?

Argon atoms are neutral, so they do not interact via Coulomb interaction. However, small displacements between the nucleus and the electron cloud gives rise to a small dipole moment. Overall, the interaction between dipoles gives an attractive interaction that scales as $`1/r^6`$ where $`r`$ is the distance between atoms.

On the other hand, Argon atoms do not come so close to each other that they overlap (they do not form molecules), hence there must be a repulsive force at small distances. 

Both of these aspects are parametrized in the Lennard-Jones potential, which takes the form

```math
U(r) = 4\epsilon \left(\left(\frac{\sigma}{r}\right)^{12} - \left(\frac{\sigma}{r}\right)^6 \right)
```

For Argon the fitting parameters have been determined as $`\epsilon/k_B=119.8\,\mathrm{K}`$ and $`\sigma = 3.405\,\text{Angstrom}`$.

### In which space do the Argon atoms sit?

We want to simulate an infinite system of Argon atoms - but obviously, a computer cannot simulate infinite space.

In practice, we will thus use a finite box of length $`L`$, with periodic boundary conditions (to emulate an infinite system).

The periodic boundary condition however does lead to some subtle aspects:

- If a particle leaves the box, it reenters at the opposite side
- The evaluation of the interaction between two atoms is also influenced by the periodic boundary conditions, as
  they introduce copies of the particles around the simulation box:
  <img src="PBC.png" width="300">

  The smallest distance between particles may thus not be the distance within the box! Here we choose
  the convention to choose the pair that has the shortest distance.
  
### How do the particles evolve in time?

The motion of the particles is governed by Newton's equation:

```math
m \frac{d^2\mathbf{x}}{dt^2} = \mathbf{F}(\mathbf{x}) = - \nabla U(\mathbf{x})
```

(a little reminder: when we have a potential that only depends on the distance $`r=\sqrt{x^2+y^2+z^2}`$, we have
$`\nabla U(r) = \frac{dU}{dr} \frac{\mathbf{r}}{r}`$)

These equations cannot be solved exactly, but need to be solved approximatelt numerically. Instead of continuous time, we thus will evolve the particles with finite timesteps $h$.

A very simple (in fact, eventually we will see too simple) way is to use the following scheme derived from the 
Newton equation. If $`\mathbf{x}_n`$ is the position and $`\mathbf{v}_n`$ are the positions and velocity at time $`t_n`$, the position and velocity at the next time $`t_{n+1} = t_n + h`$ are given by

```math
\begin{aligned}
\mathbf{x}_{n+1} & = \mathbf{x}_n + \mathbf{v}_n h \\
\mathbf{v}_{n+1} & = \mathbf{v}_n + \frac{1}{m} \mathbf{F}(\mathbf{x}_n) h
\end{aligned}
```

### What to do

Play around with this system. Start by simulating the time evolution of a few particles in a periodic box, add the forces due to the Lennard-Jones potential. Check how the total energy of your system evolves over time.

It's easier to start with a 2D system, but plan to switch to 3D at a later stage.

## Units

When you start coding the simulation of the motion of the Argon atoms, you soon find that the normal SI units
(kg, m, s, etc) lead to numbers of quite different magnitude in the simulations: e.g. mass of the Argon atoms
$`\sim 6.6 \times 10^{-26}\,\text{kg}`$ or distances of order Angstrom $`\sim 10^{-10}\,\text{m}`$.

Working with these different orders of magnitude is cumbersome, and prone to round-off errors. It is hence useful to identify some natural units in the simulation.

From the Lennard-Jones potential we can already find two natural units: $`\sigma`$ for position, and $`\epsilon`$ for the energy. Let us thus define $`\tilde{\mathbf{x}} = \mathbf{x}/\sigma`$ (and hence also $`\tilde{r} = r/\sigma`$). We can then define a dimensionless Lennard-Jones potential as

```math
\tilde{U}(\tilde{r}) = U(r)/\epsilon = 4 ( \tilde{r}^{-12} - \tilde{r}^{-6})
```

Now let's derive a the Newton equation for the dimensionless position:

```math
\frac{d^2 \tilde{\mathbf{x}}}{dt^2} = \sigma^{-1} \frac{d^2\mathbf{x}}{dt^2} =
-\sigma^{-1} \frac{1}{m} \nabla U(r) = -\frac{\epsilon}{m\sigma} \nabla \tilde{U}(\tilde{r}) = -\frac{\epsilon}{m\sigma^2} \tilde{\nabla} \tilde{U}(\tilde{r})
```

The only variable that still has a dimension is time $`t`$. If we define also a dimensionless time
$`\tilde{t} = t/(\frac{m\sigma^2}{\epsilon})^{1/2}`$, then the whole Newton equation becomes very simple:

```math
\frac{d^2\tilde{x}}{d\tilde{t}^2} = - \tilde{\nabla} \tilde{U}(\tilde{r})
```

In practice, one usually leaves away the ~'s and just says: length is in units of $`\sigma`$, energy in units
of $`\epsilon`$ and time in units of $`(\frac{m\sigma^2}{\epsilon})^{1/2}`$.

The advantages of this approach are:

- Less cumbersome numbers/constants/units to take care of in the program (less error prone)
- Simpler equations (easier to code)
- Insight into what are the expected length/time scales in our system.

As for the last point: if we put the numbers for Argon in the units of time, we arrive at $`(\frac{m\sigma^2}{\epsilon})^{1/2} = 2.15\times 10^{-12}\,\text{s}`$. To check if this is a typical time for our system, let's double check with the expected velocities in our system. 

From the equipartition theorem we have $`v = \sqrt{3 k_B T/m}`$. The natural unit of velocity in our units is
$`\sigma/(\frac{m\sigma^2}{\epsilon})^{1/2} = \sqrt{\epsilon/m}`$. Hence, in dimensionless units

```math
v/\sqrt{\epsilon/m} = \sqrt{3 k_B T/\epsilon} \sim \text{order unity}
```
This is due to the fact that typically we will work with temperatures on the order of 100K or more (room temperature) and the fact that $`\epsilon/k_B \approx 100K`$.
Hence, the particle will move a typical distance of order $`\sigma`$ in time $`(\frac{m\sigma^2}{\epsilon})^{1/2}`$. For our dimensionless units this thus means that the time step $`h`$ should be smaller than 1, and of order $`10^{-2}-10^{-3}`$. 

### What to do

- Derive the expression of the kinetic energy in dimensionless units
- Write a molecular dynamics code that uses dimensionless units and simulate a few atoms. Plot both kinetic, potential and total energy.

### Some hints on the minimal image convention

We discussed earlier that our periodic boundary conditions also influence
how the forces are calculated. In particular, we chose the convention that
we will take only the force due to the nearest image of other particles.

Naively, it would seem that thus to compute the force on particle
$`\mathbf{x}_i`$ due to particle $`\mathbf{x}_j`$, we have to consider
all the images of $`j`$ in 3D. This would give us 27 possibilities to check
in total. However, since we are working with a orthogonal coordinate system,
there is a significant simplification:

We need to minimize the distance $`r_{ij}=\sqrt{(x_i-x'_j)^2+
(y_i-y'_j)^2+(z_i-z'_j)^2}`$, where $`x'_j, y'_j, z'_j`$ are the coordinates
of the original paritcle $`j`$ or an image, i.e. for example
$`x'_j = x_j`$, $`x_j+L`$, or $`x_j-L`$,. However, since we can shift each
of the coordinates individually, we can minimize $`r_ij`$ by minimizing
each coordinate direction separately! Instead of 27 possibilities we thus
just need to check 9.

The problem simplifies even more, since now we are effectively dealing with
1D problems. In particular we know that for a box of length $`L`$

```math
\begin{aligned}
-L/2 < x_i - x_j < L/2 &\rightarrow x'_j = x_j\\
x_i - x_j > L/2 &\rightarrow x'_j = x_j + L\\
x_i-x_j < -L/2 &\rightarrow x'_j = x_j - L
\end{aligned}
```

where $x'_j$ is now the closest image. Conditions like this are easily
evaluated with numpy arrays. Another, even more compact formulation of
the same is to write:

```python
(xi - xj + L/2) % L - L/2
```

If you want to sue this formulation, contemplate on why this does work
(and check if we made a mistake ;) ).  Use the formula that you are
comfortable with!

### Better time propagation

The normal Euler methods for integrating ordinary differential equations (ODEs) have a major downside for the molecular dynamics simulations
we are doing: they do not very well conserve the energy. But our goal is to simulate a system with conserved energy (the *microcanonical* ensemble),
so we should look for a better algorithm.

#### The Verlet algorithm

The first of such algorithms was already introduced in the very beginning of molecular dynamics (see the classic
paper of Verlet of 1967). It directly makes use of the fact that we have a second order ODE:

```math
\frac{d^2 \mathbf{x}}{dt^2}= \mathbf{F}(\mathbf{x}(t))
```

(we work in dimensionless units!) For this we can very easily derive a finite time-step algorithm by using the  Taylor expansion of $`\mathbf{x}(t\pm h)`$:

```math
\begin{aligned}
\mathbf{x}(t+h) = & \mathbf{x}(t) + h \dot{\mathbf{x}}(t) + \frac{h^2}{2} \ddot{\mathbf{x}}(t) 
+ \frac{h^3}{6} \frac{d^3}{dt^3}\mathbf{x}(t) + \mathcal{O}(h^4)\\
\mathbf{x}(t-h) = & \mathbf{x}(t) - h \dot{\mathbf{x}}(t) + \frac{h^2}{2} \ddot{\mathbf{x}}(t) 
- \frac{h^3}{6} \frac{d^3}{dt^3}\mathbf{x}(t) + \mathcal{O}(h^4)
\end{aligned}
```

Adding those two equations together and using $`\ddot{\mathbf{x}}=\mathbf{F}`$, we find

```math
\mathbf{x}(t+h) = 2 \mathbf{x}(t) - \mathbf{x}(t-h) + h^2 \mathbf{F}(\mathbf{x}(t))
```

In the first time-step, we have no information about $`\mathbf{x}(t-h)`$, in this case we need to replace it
with $`\mathbf{x}(t-h) = \mathbf{x}(t) - h \mathbf{v}(t) +\mathcal{O}(h^2)`$.

In this version of the Verlet algorithm, only the positions enter. Velocities (that we need for the kinetic energy!) have to be estimated from

```math
v(t) = \frac{\mathbf{x}(t+h) - \mathbf{x}(t-h)}{2h} + \mathcal{O}(h^2)
```

#### Velocity-Verlet

The above algorithm can be slightly altered to use also velocities directly. For that we take the Taylor expansion
of $`\mathbf{x}(t+h)`$ and replace $`\dot{\mathbf{x}} = \mathbf{v}`$ and $`\ddot{\mathbf{x}}=\mathbf{F}`$ to arrive at

```math
\mathbf{x}(t+h) = \mathbf{x}(t) + h \mathbf{v}(t) + \frac{h^2}{2} \mathbf{F}(\mathbf{x}(t))
```

Additionally, we need now an equation for $`\mathbf{v}`$. We now do a Taylor expansion for $`\mathbf{v}`$ (you see, in the end it's always Taylor expansion!)

```math
\mathbf{v}(t+h) = \mathbf{v}(t) + h \dot{\mathbf{v}}(t) + \frac{h^2}{2} \ddot{\mathbf{v}}(t) + \mathcal{O}(h^3)
```

We know that $`\dot{\mathbf{v}} = \mathbf{F}`$ but we don't know $`\ddot{\mathbf{v}}`$. So for this we Taylor expand (again!!) 

```math
\dot{\mathbf{v}}(t+h) = \dot{\mathbf{v}}(t) + h \ddot{\mathbf{v}}(t) + \mathcal{O}(h^2)
```

from which we find $`\ddot{\mathbf{v}}(t) = \frac{1}{h} (\mathbf{F}(\mathbf{x}(t+h)) - \mathbf{F}(\mathbf{x}(t))`$. Inserting this back we finally find the velocit-Verlet algorithm:

```math
\begin{aligned}
\mathbf{x}(t+h) = & \mathbf{x}(t) + h \mathbf{v}(t) + \frac{h^2}{2} \mathbf{F}(\mathbf{x}(t))\\
\mathbf{v}(t+h) = & \mathbf{v}(t) + \frac{h}{2} \left(\mathbf{F}(\mathbf{x}(t+h)) + \mathbf{F}(\mathbf{x}(t)\right)
\end{aligned}
```

This can be slightly rewritten in order to use as less memory as possible. For this please refer to 
[Jos Thijssen's book chapter on Molecular dynamics](background_reading/Molecular%20dynamics.pdf).

#### Why are these algorithms actually better?

The difference to the Euler methods is actually very subtle, if you look for example at the velocity-Verlet algorithm. It turns out that these methods
are part of a class of so-called *symplectic integrators* that are particularly suited for the time-evolution of Hamiltonian systems (which is the case
for classical mechanics as we use here). If you want to learn more, we refer you to 
[Jos Thijssens' book](background_reading/Molecular%20dynamics.pdf) 
or [Wikipedia](https://en.wikipedia.org/wiki/Symplectic_integrator)



### Initial conditions and temperature

#### Positions

The choice for the initial conditions is inflenced by two aspects:

1. We do not want the particles to start too close to each other, otherwise
the $`r^{-12}`$ repulsive potential will give a huge energy. Hence,
putting particles on some sort of regular grid is sensible.

2. Eventually, we want to simulate phase transitions, for example
solid to liquid. Argon in the solid form forms a fcc lattice. So we better
choose a number of particles that is compatible with a fcc lattice (especially
since we have periodic boundary conditions). This is easiest fulfilled
by putting the initial positions on a fcc lattice.

#### Velocities and temperature

As many of you already did, the initial velocities should be chosen such that we obtain a Maxwell distribution for temperature $`T`$. This is obtained by chosing the $`x`$, $`y`$, and $`z`$ coordinates according to a Gaussian
distribution

```math
e^{-m v_{x,y,z}^2/(2 k_B T)}
```

(Properly normalized, of course!).

It might seem thus that we have set the temperature of our system right from the start. But, in fact, we didn't: when the simulation runs it equilibrates and energy is exchanged between kinetic and potential energy and it is 
very hard to predict how this will happen.

The solution is to first let the system equilibrate for a while and then *force* the kinetic energy to have a value corresponding to a certain temperature. We do this by rescaling the velocities:

```math
\mathbf{v}_i \rightarrow \lambda \mathbf{v}_i
```

with the same parameter $`\lambda`$ for all particles. From the equipartition theorem we know that

```math
E_\text{kin} = (N-1) \frac{3}{2} k_B T
```

where there is a small subtlety that we have $`N-1`$ instead of $`N`$. The reason is that in the equipartion theorem we need to count the number of independent degrees of freedom. However, the total velocity in our system is conserved:

```math
\frac{d}{dt} \sum_i \mathbf{v}_i = \sum_{i\neq j} \frac{1}{m} \mathbf{F}_{ij} = 0
```

as the sum over all forces from the pair interaction must vanish.

The equipartition theorem thus gives the following rescaling factor $`\lambda`$:

```math
\lambda = \sqrt{\frac{(N-1) 3 k_B T}{\sum_i m v_i^2}}
```

The equilibration and rescaling needs to be repeated until the temperature is converged.

**Note:** The formulas above are *not* in dimensionless units yet! To use it in a simulation with dimensionless units
you need to convert it to dimensionless units. This is a good exercise, if you have problems with it contact us!


### Observables

After having implemented all of the above steps, you now have a program that simulates the microscopic
behavior of Argon atoms. Our goal is now to quantify their behavior, and compute macroscopic
observables.

To this end we have to perform statistical averages, denoted as $`\langle ... \rangle`$.
In statistical mechanics, this would correspond to an average over all the possible
realizations in an ensemble. In our molecular dynamics simulation, this is replaced by
an average over simulation time.

We only give bare formulas here, and refer to the book "Computational Physics"
and the references therein for details.

#### Pair correlation function

A good measure for the behavior of mass of many moving particles is the pair correlation function.
It gives the probability, given a reference particle, to find another particle at a distance $`r`$.

In the simulation, you can compute the pair correlation by making a histogram of particles
$`n(r)`$ of all pairs within a distance $`[r, r+\Delta  r]`$, with
bin size $`\Delta r`$. The pair correlation function is then given as:

```math
g(r) = \frac{2V}{N(N-1)} \frac{\langle n(r) \rangle}{4 \pi r^2 \Delta r}
```

where $V$ is the volume of the simulation cell, and $N$ the number of particles.

The pair correlation function has very specific forms in the different states of
matter.

#### Pressure and specific heat

Two well-behaved macroscpic quantities are pressure and specific heat.

Pressure $`P`$ can be computed as
```math
\frac{\beta P}{\rho} = 1 - \frac{\beta}{3 N} \left<\frac{1}{2} \sum_{i,j} r_{ij} \frac{\partial U}{\partial r_ij} \right>
```
where $`\beta=1/k_B T`$, $`\rho`$ is the density, and $`N`$ the number of particles.
$`r_ij`$ are the distances between a pair of particles.

(Note: this expression make use of the virial, which usually is written as $`\left<\sum_i \mathbf{r}_i \mathbf{F}_i\right>`$. While
this expression is correct in the infinite system limit, it has troubles with periodic boundary conditions. This problem
is solved in the expression above, where only relative distances enter.)

The specific heat $C_V$ can be computed through
```math
\frac{\langle \delta K^2 \rangle}{\langle K \rangle^2} = \frac{2}{3N}
\left(1-\frac{3N}{2 C_V}\right)
```
where $`K`$ is the kinetic energy, and $`\langle\delta K^2\rangle=
\langle K^2 \rangle - \langle K \rangle^2`$ the fluctuations of the
kinetic energy.

#### Diffusion

Another useful quantity to compute is how individual atoms move over time.
In particular, it can be insightful to compute $`\langle (x(t)-x(0))^2\rangle`$
as a function of time.

### Estimating errors

We can use the molecular dynamics simulations to calculate the expectaiton value of sevaral quantities. When we do
the averaging, we can of course only go to a finite time, instead of time $`\infty`$. 
Hence, the average is only an *estimate* of the expectation value. To assess how accurate the estimate is,
we need to compute the error (standard deviation).

If we had statistically independent random data, we could compute the standard deviation of an average $`\langle A\rangle`$
with the well-known formula $`\sigma_A = \sqrt{\frac{1}{N-1} \left(\langle A^2 \rangle - \langle A \rangle^2\right)}`$.
With molecular dynamics, we are also generating a sequence of random data - but this data is not stastistically independent,
it is *correlated*: each new configuration is generated from the previous one and thus has a memory.

This memory can be quantified by the autocorrelation function:
```math
\chi_A(t) = \int dt' \left(A(t') - \langle A \rangle \right) \left(A(t' + t) - \langle A \rangle\right)
```
that compares the fluctuations at a certain time distance. Typically the autocorrelation function has an
exponential decay $`e^{-t/\tau}`$, where $`\tau`$ is the correlation time of the simulation. To get statistically
independent data, one would thus have to take snapshots with a waiting time of more than $`\tau`$ (it turns out that
$`2 \tau`$ is a good choice). If you want to calculate the autocorrelation function from your discrete data, you can
use the formula
```math
\chi_A(t) = \frac{1}{n_{max} - t} \sum_{n=0}^{n_{max}-t} A(n) A(n+t) - \frac{1}{n_{max}-t} \sum_{n=0}^{n_{max}-t} A(n) \times \frac{1}{n_{max}-t} \sum_{n=0}^{n_{max}-t} A(n+t) 
```
To get $`\tau`$ you would then have to fit $`\chi_A(t)`$ to $`e^{-t/\tau}`$.

There are also alternative ways of how to compute the error that avoid the calculation of the autocorrelation function:

#### Data blocking

This method is described in chapter 7.4 of [the book](background_reading/Review statistical mechanics.pdf).

#### Bootstrap

Bootstrapping is a resampling method: Instead of calculating a quantitiy from the simulation data directly, we generate new random sets from it:

Say, you have $`N`$ data points. Then you make a new random set by drawing $`N`$ random data points from the original set. This is not just a reshuffling,
but you will pick certain data points several times, and others not at all. From this new data set you then compute in the usual way 
the quantity $`A`$ you want. You repeat this procedure $`n`$ times. Then you have a set of $`n`$ values for $`A`$, and from this you can compute
an estimate of the standard deviation as 
```math
\sigma_A = \sqrt{\langle A^2 \rangle - \langle A \rangle^2}
```
where the average is now over the $`n`$ data points for $`A`$ Note that there is *no* factor $`1/(n-1)`$ here - it shouldn't be, because the error shouldn't 
get smaller by doing more resampling (it is determined by the original set). For a large enough $`n`$ the error will thus be independent of $`n`$. 
