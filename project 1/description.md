# Project 1 - Molecular dynamics simulation of Argon atoms

## Introduction

In your daily life you encounter different phases of matter all the
time: Water can be in the form of ice covering lakes and canals, it is
fluid when it comes out of the tap, and it turns to vapor when you boil
it.

In this assignment you will write a simulation code to explore
different phases of matter quantitatively for a simpler system: Argon
atoms (water is surprisingly complicated and still an on-going field
of research). Through these simulations you will be able to experience
the physics of different phases and investigate their behavior in a
systematic and quantitative way.

## Instructions

### What we expect you to do

Write a well-structured computer code performing a molecular dynamics
simulation of Argon atoms. This code should be flexible and allow
for different ambient conditions (such as density or temperature) to
explore different phases of Argon. The recommended programming language
for the assignment is Python, but you may use any computer language you like.

Validate the correctness of your code, and run simulations for Argon
in different phases. Investigate these phases quantitatively, for
example using pair correlation functions, calculating pressure or
specific heat, or exploring diffusion properties. Note that while
there is a standard set of things you *can* do, the assignment is
completely open to your creativity. We definitely encourage you to
also explore things according to your own ideas and surprise us!

You will then summarize your findings in a report that is written
according to scientific standards.

### Milestones along the path

Writing a molecular dynamics simulation code is of course a formidable
task - we will guide in the lectures towards this goal, by covering
the elements you should focus on first. Along this way you will most
certainly go astray and possibly make wrong choices - this is an
integral part of the learning experience. However, we will be there to
help you get back on track!

You will find it likely helpful to reach certain milestones on your
way towards the end goal of a full-fledged simulation (though you
may deviate from this pre-defined path as long as you arrive at the end goal!):

1. Code that allows to propagate a few particles according to Newton's
   equations of motion with a pair-wise interaction. The particles
   live in a simulation box with periodic boundary conditions and
   the pair-wise interactions is evaluated according to the minimum
   image convention<br>
   Examples of questions you need to address:
   - What do you need to store the state of the system? What is the best
     data structure to this end?
   - How to integrate the equations of motion numerically?
   - How do you efficiently implement periodic boundary conditions and
     the minimum image convention?

2. Code that allows to simulate a considerable amount of particles
   (100 - 1000) while numerically conserving the total energy of the system<br>
   Examples of questions you need to address:
  - How to compute observables such as the energy?
  - Which algorithms are beneficial for energy conservation?
  - How to avoid using loops, and implementing (parts) of the algortihms
    for example in Numpy?

3. Code that is flexible enough to run simulations for different conditions
   and that can compute different observables.<br>
   Examples of questions that you need to address:
   - How to set the initial conditions (positions and velocities) of
     the particles?
   - How to set the temperature in your simulation?
   - How to organize your code such that it is easy to change the
     parameters/loop over them/compute different observables?
   - How to compute various observables, such as pair correlation functions,
     pressure, and specific heat?

4. Run simulations and obtain results that obey scientific standards.<br>
   Examples of questions you need to address:
   - How to compute error bars of observables?
   - How to validate your final code for correctness?
   - What choice should you make for your final calculations given
     that there is a deadline coming up?

### Final simulations

Simulate Argon in three different states of matter. A good starting
point are the following conditions (all given in dimensionless units):
density $`\rho=0.3`$ and temperature $`T=3`$ (gas), $`\rho=0.8`$ and
$`T=1`$ (fluid), and $`\rho=1.2`$ and $`T=0.5`$ (solid).

There is a variety of observables you can calculate: the pair correlation
function, pressure, specific heat or diffusion properties. Also, don't forget
to give error estimates! Given the limited amount of time you will have
to make a choice of what to simulate.

You can also use [Chapter 8.3 of "Computational
Physics"](background_reading/Molecular dynamics.pdf) and [the original
paper by Verlet](background_reading/Classic paper by Verlet.pdf) as
inspiration. Those documents are also a source of reference values
that you can use to test the validity of your code.

## Resources

- Materials covered in the lectures. These are also summarized in the
  lecture notes.
- [Chapters 7](background_reading/Review statistical mechanics.pdf)
  and [8](background_reading/Molecular dynamics.pdf) from the book
  "Computational Physics" by Jos Thijssen (can be found as pdf in
  `background_reading`)
- [The original paper by Verlet](background_reading/Classic paper by Verlet.pdf)
  on the molecular dynamics simulation of Argon.
- Feel free to search for any help/code snippets/ideas you can find on
  the internet. We definitely encourage you to use Numpy/Scipy/...!

## Products

* A report containing:
  - a summary of the theoretical background of the simulations,
  - relevant information about your implementation (including validation),
  - a systematic and structured overview of your simulations, and
    a critical discussion of the physics you find.
* The final version of your code.

## Assessment criteria

You can find a detailed specification of what we expect from you [here](assessment_criteria.md)

## Supervision and help

* The lecturers and the TAs are present during the lectures and are
  willing to help with all your problems
* Out of class you are encouraged to ask any question on the course
  chat (preferred), write an email to the lecturers/TAs, or drop by the office.

## Submission and feedback

**Submission date for report and code: March 29**

Please submit your report and the code to the teachers and the teaching assistant via email.
Submit
* the report as a PDF file
* We expect the code to be handed in as both a set of Python files and a PDF file ready to print. 
  The Python code will be used to see whether your code runs and gives correct results. The PDF file
  will be used for grading, and should be typeset using LaTeX and the listings or minted packages,
  and must include line numbers. Keywords should be somehow highlighted (the default options are fine),
  but don't go overboard with the colours. (You can find an example [here](howtos/typesetting.md))

If you submit a draft version of your report to the lecturers before March 26,
we will give you a short feedback containing the most important aspect to
improve in your report before March 27.