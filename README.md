## Schedule and deadlines

[Schedule of the course and deadlines for projects](schedule.md)

**IMPORTANT:** If you decide to only take the 3ECTS variant of the course, please tell us explicitly!


## Details of the projects

#### Project 1

[Background reading material](project 1/background_reading)

[Project description](project 1/description.md)

[Lecture notes](project 1/lecture_notes.md)

[Assessment criteria](project 1/assessment_criteria.md)

#### Project 2

[Background reading material](project 2/background_reading)

[Project description](project 2/description.md)

[Lecture notes](project 2/lecture_notes.pdf)

#### Project 3

For the final project, you can be inspired by [this list of possible projects](final_project/brief_project_description.md) (background literature for all
those projects is avalable in [this folder](final_project)).

But you may also define your own project! In this case, please discuss feasibility with the lecturers.

## Useful optional resources

* There is a chat channel dedicated to the course: https://chat.quantumtinkerer.tudelft.nl/compphys-leiden
* You may use also the computing server we provide at https://compphys.quantumtinkerer.tudelft.nl
  if for some reason you have problems using your own computer. To log in there, you need to set up an account
  on https://gitlab.kwant-project.org
* If you want to install Python locally on your computer, we recommend the [Anaconda distribution](https://www.anaconda.com/distribution/)
* [Resources for learning python and git](howtos/casimir_prog_course.md)
* [Instructions on typesetting your code with latex](howtos/typesetting.md)